html_renderer = require "slate.html.renderer"
gemini_renderer = require "slate.gemini.renderer"
gemini_parser = require "slate.gemini.parser"
slate_document = require("slate.document")
print_table = require("slate.table_utils").print_table
deep_copy = require("slate.table_utils").deep_copy
slate_uri = require("slate.uri")
slate_ini = require("slate.ini")

posix_stat = require("posix.sys.stat")
posix_dirent = require("posix.dirent")

local debug_file_io = false

-----------------------------------------------------------
--- Helper functions

function read_file(filename)
	if debug_file_io then print("read_file", filename) end
	local f = io.open(filename, "r")
	if not f then return nil end
	local result = f:read("a")
	f:close()
	return result
end

local function get_parent_directory(filename)
	return filename:match("^(.-)/+[^/]+/*$")
end

function make_directory(dirname)
	local ok,_,errcode = posix_stat.mkdir(dirname)
	-- Parent directory does not exist
	if not ok and errcode == 2 then
		ok = make_directory(get_parent_directory(dirname))
		if ok then
			--retry after creating parent
			ok = posix_stat.mkdir(dirname)
		end
	end
	-- simple conversion to boolean
	return not not ok
end

function write_file(filename, data)
	if debug_file_io then print("write_file", filename) end
	make_directory(get_parent_directory(filename))
	local f = io.open(filename, "w+")
	if not f then return false end
	f:write(data)
	f:close()
	return true
end

function list_directory(path, include_hidden)
	local ok,listing = pcall(posix_dirent.dir, path)
	if not ok then return nil, listing end
	out = {}
	for _,file in ipairs(listing) do
		if file ~= "." and file ~= ".." then
			if include_hidden or file:match("^[^%.]") then
				out[#out+1] = file
			end
		end
	end
	return out
end

function copy_file(from_path, to_path)
	if debug_file_io then print("copy_file '"..from_path.."' > '"..to_path.."'") end
	make_directory(get_parent_directory(to_path))
	-- better than previous approach, probably fine here as files should remain small
	local data = read_file(from_path)
	if not data then return false end
	return write_file(to_path, data)
end

local function read_gemini_document(name)
	if not name:match("%.%.") then
		local text = read_file(name)
		if text then
			local document = gemini_parser.semantic_gemtext_elements_to_tree(gemini_parser.parse_gemini_markup(text))
			if #document.children == 1 then
				local child = document.children[1]
				if child.type == "section" then
					child.type = "document"
					child.useclass = nil
					child.uri = name
					return child
				end
			end
			document.uri = name
			return document
		else
			error("Couldn't read file for gemini document of "..name)
		end
	end
	return nil
end

local function read_ini_file(name)
	local text = read_file(name)
	if not text then return nil end
	return slate_ini.parse(text)
end

-----------------------------------------------------------
--- Actual site building

local env = {
	to_html_tree = html_renderer.default_renderer_settings.to_html_tree,
	rich_html_tags = html_renderer.default_renderer_settings.rich_html_tags,
	html_css_links = {
		"/assets/html_theme.css",
	},
	to_gemtext_elements = gemini_renderer.default_renderer_settings.to_gemtext_elements,
	rich_gemtext_patterns = gemini_renderer.default_renderer_settings.rich_gemtext_patterns,
	default_language = "en-US",
}

local function read_for_including(name, arg)
	local arg = arg or {}
	arg.recursion_counter = (arg.recursion_counter or 0)+1
	local document = read_gemini_document(name)
	if document then
		document = slate_document.document_to_article(document)
		if arg.recursion_counter < (arg.max_recursion or 5) then
			slate_document.replace_nodes(document, slate_document.include_replace_function, {loader = read_for_including, loader_arg = arg}, recusion_counter)
		end
	end
	arg.recursion_counter = (arg.recursion_counter or 1)-1
	return document
end

local function include_media(element, arg)
	local arg = arg or {}
	assert(arg.resources, "The include_media() replacement function needs the resources table as argument 'resources'.")
	if element.type == "paragraph" and (not element.useclass) and #(element.children or {}) == 1 then
		local text = element.children[1]
		if type(text) ~= "string" then return end
		local name,decorationclass = text:match("^%$media%s+([^%s]+)%s*([^%s]*)")
		if name then
			if decorationclass == "" then decorationclass = nil end
			local resource_metadata = arg.resources[name]
			assert(resource_metadata,"Resource '"..name.."' gets requested but does not exist!")
			local resource_description = resource_metadata.resource
			resource_description.is_used = true
			assert(resource_description, "Resource '"..name.."' has no description!")
			if arg.mode == "gemini" then
				if resource_description.gemini_inline_preview then
					local inline_preview = read_gemini_document("resources/"..name.."/"..resource_description.gemini_inline_preview)
					assert("Failed to load gemini_inline_preview for resource '"..name.."'")
					inline_preview.useclass = nil
					inline_preview.decorationclass = decorationclass
					return inline_preview
				elseif resource_description.gemini_preferred_file or resource_description.preferred_file then
					return {
						type = "paragraph",
						useclass = "link",
						uri = "/resources/"..name.."/"..(resource_description.gemini_preferred_file or resource_description.preferred_file),
						text = resource_description.title or resource_description.description,
						decorationclass = decorationclass,
					}
				end
			end
			return {
				type = "media",
				useclass = resource_description.class,
				uri = "/resources/"..name.."/"..resource_description.preferred_file,
				title = resource_description.title,
				description = resource_description.description,
				decorationclass = decorationclass,
			}
		end
	end
end

local function write_export_file(name, data)
	if not name:match("%.%.") then
		write_file("output"..name, data)
	end
end

local function export_html(document, env, filepath)
	local text = html_renderer.render_tree(html_renderer.widget.widget(document, env))
	if text then
		write_export_file(filepath, text)
	end
end

local function export_gemini(document, env, filepath)
	local text = gemini_renderer.render_elements(gemini_renderer.to_gemtext_elements.widget(document, env))
	if text then
		write_export_file(filepath, text)
	end
end

local function add_link_names(element, arg)
	local arg_is_table = type(arg) == "table"
	if type(element) == "table" then
		if element.type == "paragraph" and element.useclass == "link" then
			if type(element.children) == "table" then
				if #element.children == 0 then
					if arg_is_table then
						element.children[1] = arg[element.uri]
					else
						element.children[1] = arg(element.uri)
					end
				end
				if #element.children == 0 then
					element.children[1] = element.uri
				end
			end
		end
	end
end

local function add_category_indices(element, arg)
	local arg = arg or {}
	if not arg.root_category then return end
	if element.type == "paragraph" and type(element.children) == "table" and #(element.children or {}) == 1 then
		if not type(element.children[1]) == "string" then return end
		local category_path = element.children[1]:match("^%$category_index ([^%s]+)$")
		if category_path or element.children[1] == "$category_index" then
			local category_path = category_path or arg.category_path
			category_path = category_path:gsub("^%./", arg.category_path.."/")
			print(element, category_path)
			local category = arg.root_category
			for token in category_path:gmatch("[^/]+") do
				print(token)
				category = category.categories[token]
				if not category then return end
			end
			local listing_children = {}
			local element = {
				type = "list",
				children = listing_children,
			}
			for slug,article in pairs(category.articles) do
				if slug ~= (category.metadata.index or "index") then
					listing_children[#listing_children+1] = {
						type = "paragraph",
						useclass = "link",
						uri = (category.metadata.path or category_path).."/"..slug,
						children = {
							( article.config.index_title or article.content.title ),
						}
					}
				end
			end
			table.sort(listing_children, function (a,b) return a.uri < b.uri end)
			return element
		end
	end
end

-----------------------------
--- Read Resource Metadata

local resource_list = list_directory("resources")

local resources = {}

assert(resource_list ~= nil, "No resources folder")

for _,name in ipairs(resource_list) do
	local resource_metadata = read_ini_file("resources/"..name.."/resource.ini")
	assert(resource_metadata, "Resource '"..name.."' has no resource.ini!")
	resources[name] = resource_metadata
end

-----------------------------
--- Create banners

local banner_list = read_ini_file("decoration/banners.ini")
local banners = {}

if banner_list then
	for name, banner in pairs(banner_list) do
		if name ~= "" and name ~= "aliases" then
			local banner_content = {
				type = "paragraph",
				useclass = (banner.link and "link"),
				uri = banner.link,
				children = {banner.message or ""},
			}
			banners[name] = {
				type = "section",
				useclass = "banner",
				decorationclass = "banner-style-"..(banner.theme or "generic"),
				children = {banner_content},
			}
		end
	end
	
	if banner_list.aliases then
		for alias,name in pairs(banner_list.aliases) do
			banners[alias] = banners[name]
		end
	end
end

-----------------------------
--- Read theme

local global_theme_config = read_ini_file("decoration/theme.ini")
assert(global_theme_config)

if global_theme_config.header then
	if global_theme_config.header["title-format"] then
		env.header_title_format = global_theme_config.header["title-format"]
	end
	if global_theme_config.header["favicon-uri"] then
		env.favicon_uri = global_theme_config.header["favicon-uri"]
		env.favicon_mimetype = global_theme_config.header["favicon-mimetype"]
	end
end

local theme_descriptions = {}
local global_html_css = ""

if global_theme_config.enabled_themes then
	for _,theme_name in ipairs(global_theme_config.enabled_themes) do
		print("Reading theme "..theme_name)
		local theme_description = read_ini_file("themes/"..theme_name.."/theme.ini")
		assert(theme_description, "The theme '"..theme_name.."' has no description file or does not exist!")
		theme_descriptions[theme_name] = theme_description
		if theme_description.theme then
			if theme_description.theme.html_stylesheet then
				assert(not theme_description.theme.html_stylesheet:match("%.%."), "Filename for stylesheet may not contain '..'!")
				local stylesheet_content = read_file("themes/"..theme_name.."/"..theme_description.theme.html_stylesheet)
				assert(stylesheet_content, "Html stylesheet not found!")
				global_html_css = global_html_css.."\n/** Styling from theme "..theme_name.." **/\n\n"..stylesheet_content.."\n"
			end
		end
		if theme_description.assets then
			for _,asset_name in ipairs(theme_description.assets) do
				assert(not asset_name:match("%.%."), "Filename for asset may not contain '..'!")
				copy_file("themes/"..theme_name.."/"..asset_name, "output/assets/"..theme_name.."/"..asset_name)
			end
		end
	end
end

write_file("output/assets/html_theme.css", global_html_css)
-----------------------------
--- Read Articles

local function read_category(category_directory)
	local category = { articles={}, categories={} }
	category.metadata = (read_ini_file(category_directory.."/category.ini") or {}).category or {}
	local article_list = list_directory(category_directory)
	for _,name in ipairs(article_list) do
		if name:match(".+%.gemini$") then
			local slug = name:match("^(.+)%.gemini$")
			local document = read_gemini_document(category_directory.."/"..name)
			local article = slate_document.document_to_article(document)
			article.index_id = "content"
			local article_config = read_ini_file(category_directory.."/"..slug..".ini") or {}
			category.articles[slug] = {content = article, config = article_config}
		elseif name:match(".+%.category$") then
			local slug = name:match("^(.+)%.category$")
			category.categories[slug] = read_category(category_directory.."/"..name)
		end
	end
	
	return category
end

local root_category = read_category("articles")

-----------------------------
--- Read footer

local footer = read_gemini_document("decoration/footer.gemini")
if footer then
	footer = slate_document.document_to_article(footer)
	footer.useclass = "footer"
end

-----------------------------
--- Read Text-Header

local text_header = read_gemini_document("decoration/text_header.gemini")
if text_header then
	text_header = slate_document.document_to_article(text_header)
	text_header.useclass = "header"
end

-----------------------------
--- Read Navigation

local navigation_description = read_ini_file("decoration/navigation.ini")
local navigation = false
if navigation_description then
	navigation = {
		type = "section",
		useclass = "navigation",
		children = {{
			type = "paragraph",
			useclass = "link",
			uri = "#content",
			children = {"Skip Site Navigation"},
			decorationclass = "skiplink",
		}}
	}
	if navigation_description.title then
		assert(navigation_description.title.title, "If you specify a title section you have to give ot ad title value!")
		navigation.children[#navigation.children+1] = {
			type = "paragraph",
			useclass = (navigation_description.title["title-link"] and "link"),
			children = {navigation_description.title.title},
			uri = navigation_description.title["title-link"],
			decorationclass = "sitename",
		}
	end
	if navigation_description.navigation_links then
		if #navigation_description.navigation_links > 0 then
			local navigation_links = {
				type = "list",
				children = {},
			}
			navigation.children[#navigation.children+1] = navigation_links
			for _,line in ipairs(navigation_description.navigation_links) do
				local uri,name = line:match("^([^%s]+)%s+(.+)$")
				if not (uri and name) then
					error("Format for navigation links is '<uri> <name>', both are mandatory!")
				end
				navigation_links.children[#navigation_links.children+1] = {
					type = "paragraph",
					useclass = "link",
					uri = uri,
					children = {name},
				}
			end
		end
	end
end

-----------------------------
--- Make Documents

local function build_article_documents(article, path, env, category)
	local gemini_document = {
		type = "document",
		uri = path,
		children = {},
	}
	if text_header then
		gemini_document.children[#gemini_document.children+1] = text_header
	end
	gemini_document.children[#gemini_document.children+1] = article.content
	if footer then
		gemini_document.children[#gemini_document.children+1] = footer
	end
	local gemini_document = deep_copy(gemini_document)
	slate_document.replace_nodes(gemini_document, include_media, {resources = resources, mode="gemini"})
	article.gemini_document = gemini_document
	--- HTML Document
	local html_document = {
		type = "document",
		uri = path,
		title = article.content.title,
		children = {},
		language = article.config.language or env.default_language
	}
	local banner = banners["default"]
	if article.config.banner then
		banner = banners[article.config.banner] or banner
	end
	banner = banners[path] or banner
	if navigation or banner then
		local children = {}
		if navigation then
			children[#children+1] = navigation
		end
		if banner then
			print_table(banner)
			children[#children+1] = banner
		end
		html_document.children[#html_document.children+1] = {
			type = "section",
			useclass = "header",
			children = children,
		}
	end
	html_document.children[#html_document.children+1] = article.content
	if footer then
		html_document.children[#html_document.children+1] = footer
	end
	local html_document = deep_copy(html_document)
	slate_document.replace_nodes(html_document, include_media, {resources = resources})
	article.html_document = html_document
end

local function build_article_indices(article, path, env, category, root_category)
	if article.gemini_document then
		slate_document.replace_nodes(article.gemini_document, add_category_indices, {
			root_category = root_category,
			category_path = category.path,
		})
	end
	if article.html_document then
		slate_document.replace_nodes(article.html_document, add_category_indices, {
			root_category = root_category,
			category_path = category.path,
		})
	end
end

-----------------------------
--- Export documents

local function export_article_documents(article)
	export_gemini(article.gemini_document, env, article.gemini_document.uri.."index.gemini")
	export_html(article.html_document, env, article.html_document.uri.."index.html")
end

-----------------------------
--- Traverse article tree

local function process_category(root_category, category, category_path, env, article_processing_function)
	local category_path = category_path or ""
	print("Processing Category "..category_path.."/ …")
	category.path = category_path
	for slug,article in pairs(category.articles) do
		local article_path = category_path.."/"..slug.."/"
		if (category.metadata.index or "index") == slug then
			article_path = category_path.."/"
		end
		print("\tProcessing article "..article_path.." …")
		article_processing_function(article, article_path, env, category, root_category)
	end
	for slug,subcategory in pairs(category.categories) do
		process_category(root_category, subcategory, category_path.."/"..slug, env, article_processing_function)
	end
end

process_category(root_category, root_category, "", env, build_article_documents)
process_category(root_category, root_category, "", env, build_article_indices)
process_category(root_category, root_category, "", env, export_article_documents)

-----------------------------
--- Copy resources

for name, resource_metadata in pairs(resources) do
	if resource_metadata.resource then
		if resource_metadata.resource.is_used then
			for description_name, description in pairs(resource_metadata) do
				local filename = description_name:match("^file:(.+)$")
				if filename then
					copy_file("resources/"..name.."/"..filename, "output/resources/"..name.."/"..filename)
				end
			end
		end
	end
end
