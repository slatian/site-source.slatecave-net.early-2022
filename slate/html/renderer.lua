local deep_copy = require("slate.table_utils").deep_copy

local function render_html(html_tree, level, prefix)
	local level = level or 0
	local indent = ("\t"):rep(level)
	if type(html_tree) == "string" then
		local text = html_tree:gsub("&","&amp;"):gsub("<","&lt;"):gsub(">","&gt;"):gsub("\n","<br>")
		return (prefix or indent)..text
	end
	if html_tree._ then
		local output = (prefix or indent).."<"..tostring(html_tree._)
		if html_tree._ == "html" then
			output = (prefix or indent).."<!DOCTYPE html>"..output
		end
		-- append keys in alphabetical order to reduce diff noise
		local keys = {}
		for k,v in pairs(html_tree) do
			if type(k) == "string" then
				local key = k:match("^([a-z%-]+)$")
				if key then
					keys[#keys+1] = key
				end
			end
		end
		table.sort(keys)
		for _,key in ipairs(keys) do
			output = output.." "..key.."=\""..html_tree[key].."\""
		end
		if #html_tree == 1 then
			output = output..">"..render_html(html_tree[1], level+1,"").."</"..tostring(html_tree._)..">"
		elseif #html_tree > 0 then
			output = output..">\n"
			for n,e in ipairs(html_tree) do
				output = output..render_html(e, level+1).."\n"
			end
			output = output..indent.."</"..tostring(html_tree._)..">"
		else
			output = output.."/>"
		end
		return output
	end
	return ""
end

local function get_class_tag_for_html(element, env)
	local classes = "element-"..tostring(element.type)
	if element.useclass then
		classes = classes.." useclass-"..tostring(element.useclass)
	end
	if element.decorationclass then
		classes = classes.." decoration-"..tostring(element.decorationclass)
	end
	return classes
end

local function render_widget_to_html_tree(widget, env)
	local env = deep_copy(env)
	if type(widget) == "string" then
		return widget
	elseif type(widget) == "table" then
		local renderer = widget.to_html_tree
		if type(env.to_html_tree) == "table" then
			renderer = env.to_html_tree[(widget.type or "*").."."..(widget.useclass or "*")] or env.to_html_tree[widget.type or "*"] or env.to_html_tree["*"] or renderer
		end
		if type(renderer) == "function" then
			ok,res = pcall(renderer, widget, env)
			if ok then
				if type(res) == "table" then
					res.class = get_class_tag_for_html(widget, env)
					res.id = res.id or widget.index_id
				end
				return res
			else
				return {_="error", "Lua error: "..tostring(res)}
			end
		elseif renderer == "" then
			return nil
		else
			return {_="error", "No rendering function for element ("..tostring(widget.type).."."..tostring(widget.useclass)..")"}
		end
	else
		return {_="error", "Node type "..type(widget).." is not supported."}
	end
end

local function add_figure(html_element, element)
	figure = {
		_="figure",
	}
	if element.title and element.title ~= "" then
		figure[#figure+1] = {
			_ = "figcaption",
			element.title,
		}
	end
	figure[#figure+1] = html_element
	if element.description and element.description ~= "" then
		figure[#figure+1] = {
			_ = "figcaption",
			element.description,
		}
	end
	return figure
end

local function document_to_html_tree(document, env)
	local env = env or {}
	local head = {
		_="head",
		{_="meta", charset="utf8"},
		{_="meta", name="viewport", content="width=device-width, initial-scale=1, maximum-scale=5.0"},
	}
	if type(env.html_css_links) == "table" then
		for _,uri in ipairs(env.html_css_links) do
			head[#head+1] = {_="link", rel="stylesheet", href=uri, type="text/css"}
		end
	end
	if type(env.favicon_uri) == "string" and type(env.favicon_mimetype) == "string" then
		head[#head+1] = {_="link", rel="icon", href=env.favicon_uri, type=env.favicon_mimetype}
	end
	local body = {_="body"}
	local out = {
		_="html",
		lang=document.language,
		head,
		body,
	}
	if document.title then
		if env.header_title_format then
			local title = env.header_title_format:gsub("{}", document.title)
			head[#head+1] = {_="title", title}
		else
			head[#head+1] = {_="title", document.title}
		end
	end
	if document.description then
		head[#head+1] = {_="meta", name="description", content=document.description}
	end
	--TODO add base tag when uri is set
	if document.children then
		for n,w in ipairs(document.children) do
			body[#body+1] = render_widget_to_html_tree(w, env)
		end
	end
	return out
end

local function section_to_html_tree(section, env)
	local out = {_="section"}
	local env = env or {}
	if section.useclass == "article" or section.useclass == "header" or section.useclass == "footer" then
		out._=section.useclass
	elseif section.useclass == "navigation" then
		out._="nav"
	end
	if section.title then
		local hlevel = env.hlevel or 0
		env.hlevel = hlevel+1
		out[#out+1] = {_="h"..tostring(hlevel+1), section.title}
	end
	if section.description then
		out[#out+1] = {_="p", class="role-description", section.description}
	end
	if section.children then
		for _,w in ipairs(section.children) do
			out[#out+1] = render_widget_to_html_tree(w, env)
		end
	end
	return out
end

local function list_to_html_tree(section, env)
	local list = {_="ul"} 
	local out = list
	if section.title then
		out = {
			_="section",
			{_="p", section.title},
			list,
		}
	end
	local is_definition_list = false
	if section.useclass == "ordered" then
		list._ = "ol"
	elseif section.useclass == "definition-group" then
		list._ = "dl"
		is_definition_list = true
	end
	if section.children then
		for _,w in ipairs(section.children) do
			if is_definition_list then
				if w.type == "list" and w.useclass == "definition" then
					list[#list+1] = {
						_="dt",
						w.title
					}
					for _,def in ipairs(w.children) do
						list[#list+1] = {
							_="dd",
							render_widget_to_html_tree(def, env)
						}
					end
				end
			else
				list[#list+1] = {
					_="li",
					render_widget_to_html_tree(w, env)
				}
			end
		end
	end
	return out
end

local function paragraph_to_html_tree(paragraph, env)
	local out = {_="p"}
	local content = out
	if paragraph.useclass == "quote" then
		out._ = "blockquote"
		out.cite = paragraph.uri
	elseif paragraph.useclass == "link" then
		content = {_="a", href=paragraph.uri}
		if env.hrml_wrap_paragraph_links then
			out[#out+1] = content
		else
			out = content
		end
	elseif paragraph.useclass == "preformatted" then
		if paragraph.decorationclass == "code" then
			out._ = "code"
		elseif paragraph.decorationclass == "sample" then
			out._ = "samp"
		else
			out._ = "pre"
		end
	end
	--TODO: add style classes
	if paragraph.children then
		if #paragraph.children > 0 then
			for n,w in ipairs(paragraph.children) do
				content[#content+1] = render_widget_to_html_tree(w, env)
			end
		else
			content[#content+1] = paragraph.uri
		end
	else
		content[#content+1] = paragraph.uri
	end
	if paragraph.useclass == "preformatted" and (paragraph.title or paragraph.description) then
		out = add_figure(out, paragraph)
	end
	return out
end

local function rich_text_to_html_tree(rich_text, env)
	local out = {_="span"}
	if env.rich_html_tags and rich_text.useclass then
		out._ = env.rich_html_tags[rich_text.useclass] or out._
	end
	if rich_text.useclass == "quote" then
		out.cite = rich_text.uri
	end
	if rich_text.useclass == "link" then
		out.href = rich_text.uri
		out._ = "a"
	end
	if rich_text.children then
		for n,w in ipairs(rich_text.children) do
			out[#out+1] = render_widget_to_html_tree(w, env)
		end
	end
	return out
end

local function thematic_break_to_html_tree(thematic_break, env)
	return {_="hr"}
end

local function image_media_to_html_tree(media, env)
	out = {_="picture"}
	--TODO add alternatives
	out[#out+1] = {
		_="img",
		src=media.uri,
		alt=media.description or media.title,
	}
	if media.decorationclass == "figure" then
		return add_figure(out, media)
	end
	return out
end

local default_renderer_settings = {
	to_html_tree = {
		document = document_to_html_tree,
		section = section_to_html_tree,
		paragraph = paragraph_to_html_tree,
		rich_text = rich_text_to_html_tree,
		["media.image"] = image_media_to_html_tree,
		thematic_break = thematic_break_to_html_tree,
		["thematic_break.weak"] = "",
		list = list_to_html_tree,
	},
	rich_html_tags = {
		empahsis_0 = "em",
		emphasis_1 = "b",
		preformatted = "pre",
		quote = "q",
		label = "b",
		idiom = "i",
	},
}

return {
	render_tree = render_html,
	get_class_tag_for_html = get_class_tag_for_html,
	render_widget_to_html_tree = render_widget_to_html_tree,
	widget = {
		widget = render_widget_to_html_tree,
		document = document_to_html_tree,
		section = section_to_html_tree,
		list = list_to_html_tree,
		paragraph = paragraph_to_html_tree,
		rich_text = rich_text_to_html_tree,
		thematic_break = thematic_break_to_html_tree,
		image_media = image_media_to_html_tree,
	},
	default_renderer_settings = default_renderer_settings,
}
