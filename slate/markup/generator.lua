local Stack = require("slate.table_utils").Stack

local function markup_start_section(this, level, title, nodetype, useclass, interpret_nodetype, uri, description, index_id)
	local interpret_nodetype = interpret_nodetype ~= false
	if type(title) == "string" and level < this.autotitle_level_limit then
		this.root_node.title = title
		this.autotitle_level_limit = level
	end
	local is_list_section = false
	if interpret_nodetype then
		is_list_section = nodetype == "list"
		if not is_list_section and this.current_list_level > 0 then
			this:set_list_level(0);
		end
	end
	if is_list_section then
		this:set_list_level(level)
	else
		this:set_section_level(level)
	end
	this.current_section_level = this.current_section_level+1
	if is_list_section then
		this.current_list_level = this.current_list_level+1
	end
	local new_section = {
		type = nodetype or "section",
		useclass = useclass,
		uri = uri,
		description = description,
		title = title,
		index_id = index_id,
	}
	this:append_node(new_section)
	this.sections:push(this.current_section)
	this.current_section = new_section
end

local function markup_set_section_level(this, level, new_section_nodetype, new_section_useclass)
	while (level > this.current_section_level) do
		this:start_section(this.current_section_level, nil, new_section_nodetype or "section", new_section_useclass, false)
	end
	while (level < this.current_section_level) do
		this.current_section_level = this.current_section_level-1;
		if (this.current_list_level > 0) then
			this.current_list_level = this.current_list_level-1;
		end
		this.current_section = this.sections:pop();
	end
end

local function markup_append_node(this, node)
	local current_section = this.current_section
	if not current_section.children then
		current_section.children = {}
	end
	current_section.children[#current_section.children+1] = node
end

local function markup_set_list_level(this, list_level, useclass)
	if (this.current_list_level ~= list_level) then
		this:set_section_level((this.current_section_level-this.current_list_level)+list_level, "list", useclass)
		this.current_list_level = list_level
	end
end

local function MarkupDocumentGenerator(title, useclass, uri, description)
	local this = {
		sections = Stack(),
		root_node = {
			type = "document",
			useclass = useclass,
			uri = uri,
			description = description,
			title = title,
		},
		autotitle_level_limit = 0,
		current_list_level = 0,
		current_section_level = 0,
		--functions
		start_section = markup_start_section,
		set_section_level = markup_set_section_level,
		append_node = markup_append_node,
		set_list_level = markup_set_list_level,
	}
	this.current_section = this.root_node
	if not title then
		this.autotitle_level_limit = 100
	end
	return this
end

return {
	MarkupDocumentGenerator = MarkupDocumentGenerator,
}
