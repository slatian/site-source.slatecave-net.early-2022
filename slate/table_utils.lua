local function deep_copy(t)
	if type(t) == "table" then
		local ot = {}
		for k,v in pairs(t) do
			ot[deep_copy(k)] = deep_copy(v)
		end
		return ot
	else
		return t
	end
end

local function stack_push(this, element)
	this[#this+1] = element
end

local function stack_pop(this, element)
	local i = #this
	local out = this[i]
	this[i] = nil
	return out
end	

local function Stack()
	return {
		push = stack_push,
		pop = stack_pop,
	}
end

local function print_table(t, pfx)
	local pfx = pfx or ""
	if type(t) == "table" then
		for k,v in pairs(t) do
			if type(v) == "table" then
				print(pfx.."-."..tostring(k))
				print_table(v, pfx.." |")
			else
				print(pfx..tostring(k).." = "..tostring(v))
			end
		end
	else
		print(pfx..tostring(t))
	end
end

return {
	deep_copy = deep_copy,
	stack_push = stack_push,
	stack_pop = stack_pop,
	Stack = Stack,
	print_table = print_table,
}
