local function parse_ini(text)
	local results = {[""] = {}}
	local current_section = results[""]
	for line in text:gmatch("[^\n]*") do
		local new_section = line:match("^%[(.-)%]$")
		if new_section then
			if results[new_section] then
				current_section = results[new_section]
			else
				current_section = {}
				results[new_section] = current_section
			end
		else
			if not (line:match("^%s*#.+$") or line:match("^%s*$")) then
				local key,value = line:match("^(.-)%s*=%s*(.-)%s*$")
				if key and value then
					current_section[key] = value
				end
				local listvalue = line:match("^%* (.+)")
				if listvalue then
					current_section[#current_section+1] = listvalue
				end
			end
		end
	end
	return results
end

return {
	parse = parse_ini
}
