local deep_copy = require("slate.table_utils").deep_copy

local function render_gemtext(elements)
	local out = ""
	for _,element in ipairs(elements) do
		if type(element) == "table" then
			local et = element._
			if et == "paragraph" then
				out = out..tostring(element.text).."\n"
			elseif et == "empty_line" then
				out = out.."\n"
			elseif et == "quote" then
				out = out.."> "..tostring(element.text):gsub("\n","\n> ").."\n"
			elseif et == "link" then
				if element.text then
					out = out.."=> "..tostring(element.uri).." "..tostring(element.text).."\n"
				else
					out = out.."=> "..tostring(element.uri).."\n"
				end
			elseif et == "preformatted" then
				out = out..("```"..tostring(element.prefix or "").."\n"..tostring(element.text or "")):gsub("\n```","\n ```").."\n```"..tostring(element.suffix or "").."\n"
			elseif et == "header" then
				out = out..("#"):rep(element.level or 1).." "..tostring(element.text).."\n"
			elseif et == "listitem" then
				out = out.."* "..tostring(element.text).."\n"
			elseif et == "error" then
				out = out.."ERROR: "..tostring(element.text).."\n"
			end
		elseif type(element) == "string" then
			out = out..element.."\n"
		end
	end
	return out
end

local function render_widget_to_gemtext_elements(widget, env)
	local env = deep_copy(env)
	if type(widget) == "string" then
		return {widget}
	elseif type(widget) == "table" then
		local renderer = widget.to_gemtext_elements
		if type(env.to_gemtext_elements) == "table" then
			renderer = env.to_gemtext_elements[(widget.type or "*").."."..(widget.useclass or "*")] or env.to_gemtext_elements[widget.type or "*"] or env.to_gemtext_elements["*"] or renderer
		end
		if type(renderer) == "function" then
			ok,res = pcall(renderer, widget, env)
			if ok then
				return res
			else
				return {{_="error", text="("..tostring(widget.type).."."..tostring(widget.useclass)..") Lua error: "..tostring(res)}}
			end
		else
			return {{_="error", text="No rendering function for element ("..tostring(widget.type).."."..tostring(widget.useclass)..")"}}
		end
	else
		return {{_="error", text="Node type "..type(widget).." is not supported."}}
	end
end

local function section_to_gemtext_elements(section, env)
	local out = {}
	local env = env or {}
	if section.title and section.title ~= "" then
		env.hlevel = (env.hlevel or 0)+1
		out[#out+1] = {_="header", level=env.hlevel, text=section.title}
	end
	if section.description then
		out[#out+1] = {_="paragraph", text=section.description}
	end
	local last_was_empty_line = true
	env.is_listitem = section.type == "list"
	if section.children then
		for _,w in ipairs(section.children) do
			if type(w) == "table" then
				if (w.type == "section" or w.type == "list") and (not last_was_empty_line) then
					out[#out+1] = {_="empty_line"}
				end
			end
			elements = render_widget_to_gemtext_elements(w, env)
			if type(w) == "string" and env.is_listitem then
				elements = {{_="listitem", text=w}}
			end
			for _,e in ipairs(elements) do
				out[#out+1] = e
				last_was_empty_line = (e._ == "empty_line")
			end
			if elements.links then
				for _,link in ipairs(elements.links) do
					out[#out+1] = {_="link", text="["..tostring(link.text).."]", uri=link.uri}
				end
				last_was_empty_line = false
			end
		end
	end
	return out
end

local function list_to_gemtext_elements(section, env)
	env.is_listitem = section.type == "list"
	local out = {}
	if section.title then
		out[#out+1] = {_="paragraph", text=section.title}
	end
	if section.children then
		for _,w in ipairs(section.children) do
			elements = render_widget_to_gemtext_elements(w, env)
			if type(w) == "string" then
				elements = {{_="listitem", text=w}}
			end
			for _,e in ipairs(elements) do
				out[#out+1] = e
			end
		end
	end
	return out
end

local function definition_list_to_gemtext_elements(section, env)
	local out = {}
	for i,w in ipairs(section.children) do
		elements = render_widget_to_gemtext_elements(w, env)
		if type(w) == "string" then
			elements = {{_="listitem", text=" - "..w}}
		end
		for j,e in ipairs(elements) do
			if j == 1 then
				if i == 1 then
					e.text = section.title.." - "..e.text
				else
					e.text = "- "..e.text
				end
			end
			out[#out+1] = e
		end
	end
	return out
end

local function render_rich_gemtext_paragraph_content(children, env, links)
	local out = ""
	for _,e in pairs(children) do
		if type(e) == "string" then
			out = out..e
		elseif type(e) == "table" then
			local text = render_rich_gemtext_paragraph_content(e.children or {}, env, links) or ""
			if e.type == "rich_text" then
				if e.useclass == "link" then
					links[#links+1] = {uri=e.uri, text=text}
				end
				if env.rich_gemtext_patterns then
					local pattern = env.rich_gemtext_patterns[e.useclass]
					if type(pattern) == "string" then
						local res = pattern:gsub("{}", text:gsub("%%","%%%%"))
						out = out..res
					elseif type(pattern) == "function" then
						out = out..tostring(pattern(text))
					else
						out = out..text
					end
				end
			end
		end
	end
	return out
end

local function paragraph_to_gemtext_elements(paragraph, env)
	local content = {_="paragraph"}
	local out = {content}
	if paragraph.useclass == "quote" or paragraph.useclass == "preformatted" then
		content._ = paragraph.useclass
	elseif paragraph.useclass == "link" then
		content._ = "link"
		content.uri = paragraph.uri
	elseif env.is_listitem then
		content._ = "listitem"
	end
	if paragraph.useclass == "preformatted" and paragraph.description then
		content.prefix, content.suffix = paragraph.description:match("^([^\n]*)\n?([^\n]*)")
		if content.prefix == "" then content.prefix = nil end
		if content.suffix == "" then content.suffix = nil end
	end
	local links = {}
	if paragraph.children then
		content.text = render_rich_gemtext_paragraph_content(paragraph.children, env, links)
	end
	if paragraph.useclass ~= "link" and #links > 0 then
		local link_content = {}
		out.links = link_content
		for _,link in ipairs(links) do
			link_content[#link_content+1] = {_="link", uri=link.uri, text=link.text}
		end
	end
	return out
end

local function media_to_gemtext_elements(media, env)
	local out = {{_="link", uri=media.uri, text=media.title or media.description}}
	if media.title and media.description then
		out[#out+1] = {_="paragraph", text=media.description}
	end
	return out
end

local function weak_thematic_break_to_gemtext_elements(element, env)
	if env.render_weak_breaks_to_gemtext ~= false then
		return {{_="empty_line"}}
	else
		return {}
	end
end

local function thematic_break_to_gemtext_elements(element, env)
	return {{_="empty_line"}, {_="empty_line"}}
end

local default_renderer_settings = {
	to_gemtext_elements = {
		document = section_to_gemtext_elements,
		section = section_to_gemtext_elements,
		list = list_to_gemtext_elements,
		paragraph = paragraph_to_gemtext_elements,
		["thematic_break.weak"] = weak_thematic_break_to_gemtext_elements,
		thematic_break = thematic_break_to_gemtext_elements,
		image_media = media_to_gemtext_elements,
		["list.definition"] = definition_list_to_gemtext_elements,
	},
	rich_gemtext_patterns = {
		preformatted = "`{}`",
		quote = "\"{}\""
	}
}

return {
	render_elements = render_gemtext,
	to_gemtext_elements = {
		widget = render_widget_to_gemtext_elements,
		section = section_to_gemtext_elements,
		list = list_to_gemtext_elements,
		paragraph = paragraph_to_gemtext_elements,
		media = media_to_gemtext_elements,
		weak_thematic_break = weak_thematic_break_to_gemtext_elements,
		thematic_break = thematic_break_to_gemtext_elements,
		definition_list = definition_list_to_gemtext_elements,
	},
	default_renderer_settings = default_renderer_settings,
}
