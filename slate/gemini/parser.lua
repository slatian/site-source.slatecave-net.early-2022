MarkupDocumentGenerator = require("slate.markup.generator").MarkupDocumentGenerator

local function gemtext_element_paragraph_to_document_tree(element)
	if element.inline_links then
		local children = {}
		local inlines = {}
		local orphans = {}
		local text = element.text
		-- find all instances of the inline links
		local i = 1
		for _,link in ipairs(element.inline_links) do
			local istart, iend = text:find(link.text, i, true)
			if istart then
				i = iend+1
				local len = iend - istart + 1
				--if there is a conflicting entity already at this position use the longer one
				inlines[#inlines+1] = {start = istart, len = len, uri = link.uri, text=link.text}
			else
				orphans[#orphans+1] = link
			end
		end
		-- turn into tree elements
		local i = 1
		for _,link in ipairs(inlines) do
			--extract prefix
			--print(i, link.start)
			if link.start ~= i then
				--print(i, link.start-i, "'"..text:sub(i, link.start).."'")
				children[#children+1] = text:sub(i, link.start-1)
			end
			children[#children+1] = {
				type = "rich_text",
				useclass = "link",
				uri = link.uri,
				children = { link.text },
			}
			i = link.start+link.len
		end
		if i <= #text then
			children[#children+1] = text:sub(i)
		end
		for _,link in ipairs(orphans) do
			children[#children+1] = " "
			children[#children+1] = {
				type = "link",
				uri = link.uri,
				children = { "["..link.text.."]" }
			}
		end
		return {
			type = "paragraph",
			children = children,
			decorationclass = element.decorationclass,
			index_id = element.index_id,
		}
	else
		return {
			type = "paragraph",
			children = { element.text },
			decorationclass = element.decorationclass,
			index_id = element.index_id
		}
	end
end

local function gemtext_elements_to_document_tree(gemtext_elements, title, uri)
	local markup = MarkupDocumentGenerator(title, nil, uri)
	local last_element_type = "_"
	local notitle = title == nil
	local definition_has_label = false
	local definition_group_open = false
	for n, element in ipairs(gemtext_elements) do
		if last_element_type ~= element._ then
			if last_element_type == "listitem" or last_element_type == "link" then
				if (element._ ~= "listitem" and element._ ~= "link") then
					if not (definition_group_open and (element.definition_label or element.definition_text)) then
						markup:set_list_level(0)
					end
				end
			end
			-- unset definition label switching to items wich can't ba part of a definition
			if definition_has_label or definition_group_open then
				if (last_element_type == "listitem" or last_element_type == "link") then
					if (element._ ~= "listitem" and element._ ~= "link") then
						definition_has_label = false
						definition_group_open = false
						markup:set_list_level(0)
					end
				end
			end
		end
		if (element.definition_label or (element.definition_text and definition_has_label)) then
			if not definition_group_open then
				markup:set_list_level(1, "definition-group")
				definition_group_open = true
			end
			if element.definition_label then
				markup:set_list_level(1)
				markup:set_list_level(2, "definition")
				markup.current_section.title = element.definition_label
				definition_has_label = true
			end
			if element.definition_text then
				local uri = ((not element.uri_is_for_definition_label) and element.uri) or nil
				markup:append_node({
					type = "paragraph",
					useclass = uri and "link",
					children = {element.definition_text},
					uri = uri,
					decorationclass = element.decorationclass,
					index_id = element.index_id
				})
			end
		elseif element._ == "header" then
			markup:start_section(element.level-1, element.text, nil, nil, nil, nil, element.description)
			markup.current_section.decorationclass = element.decorationclass
			markup.current_section.index_id = element.index_id
		elseif element._ == "listheader" then
			markup:set_list_level(1)
			markup.current_section.title = element.text
			markup.current_section.decorationclass = element.decorationclass
			markup.current_section.index_id = element.index_id
		elseif element._ == "listitem" then
			markup:set_list_level(1)
			markup:append_node(element.text)
		elseif element._ == "paragraph" then
			markup:append_node(gemtext_element_paragraph_to_document_tree(element))
		elseif element._ == "empty_line" then
			markup:append_node{
				type = "thematic_break",
				useclass = "weak",
				decorationclass = element.decorationclass,
				index_id = element.index_id
			}
		elseif element._ == "quote" or element._ == "preformatted" then
			markup:append_node{
				type = "paragraph",
				useclass = element._,
				children = { element.text },
				description = (element.prefix or "")..((element.suffix and "\n") or "")..(element.suffix or ""),
				decorationclass = element.decorationclass,
				index_id = element.index_id
			}
		elseif element._ == "link" then
			markup:set_list_level(1)
			markup:append_node{
				type = "paragraph",
				useclass = "link",
				uri = element.uri,
				children = { element.text },
				decorationclass = element.decorationclass,
				index_id = element.index_id
			}
		end
		
		last_element_type = element._
	end
	return markup.root_node
end

local function gemtext_extract_section_descriptions(gemtext_elements)
	local o = {}
	local last_was_header = false
	for n, element in ipairs(gemtext_elements) do
		if (element._ == "paragraph" and last_was_header) then
			o[#o].description = element.text
		else
			o[#o+1] = element
		end
		last_was_header = (element._ == "header")
	end
	return o
end

local function gemtext_extract_quotes(gemtext_elements)
	local o = {}
	local last_was_quote = false
	for n, element in ipairs(gemtext_elements) do
		if (element._ == "quote" and last_was_quote) then
			o[#o].text = (o[#o].text or "").."\n"..element.text
		else
			o[#o+1] = element
		end
		last_was_quote = (element._ == "quote")
	end
	return o
end

local function gemtext_parse_inline_links(gemtext_elements)
	local o = {}
	for n, element in ipairs(gemtext_elements) do
		local is_inline_link = false
		if element._ == "link" then
			local label = tostring(element.text):match("^%[(.-)%]")
			if label then
				local i = #o
				while i >= 2 do
					e = o[i]
					i = i-1
					if e._ == "empty_line" then
						--ignore
					elseif e._ == "paragraph" then
						if type(e.text) == "string" then
							if e.text:find(label,1,true) then
								is_inline_link = true
								if not e.inline_links then
									e.inline_links = {}
								end
								e.inline_links[#e.inline_links+1] = {text = label, uri = element.uri}
							end
						end
					else
						break
					end
				end
			end
		end
		if not is_inline_link then
			o[#o+1] = element
		end
	end
	return o
end

local function extract_list_headers(elements)
	o = {}
	for n, element in ipairs(elements) do
		local is_normal_element = true
		if element._ == "paragraph" and #elements > n then
			if elements[n+1]._ == "listitem" then
				is_normal_element = false
				o[#o+1] = {_="listheader", text=element.text}
			end
		end
		if is_normal_element then
			o[#o+1] = element
		end
	end
	return o
end

local function gemtext_extract_definitions(elements)
	o = {}
	local definition_enabled = true
	local nth_listitem = 0
	local is_definition_list = false
	for n, element in ipairs(elements) do
		if element._ == "paragraph" then
			nth_listitem = 0
			if element.text == "$definition parser on" then
				definition_enabled = true
			elseif element.text == "$definition parser off" then
				definition_enabled = false
			else
				nth_listitem = 0
				o[#o+1] = element
			end
		elseif definition_enabled and (element._ == "listitem" or element._ == "link") and element.text then
			if element.text:match("%s%-%s") then
				element.definition_label, element.definition_text = element.text:match("^(.-)%s+%-%s+(.+)")
				element.uri_is_for_definition_label = true
			elseif element.text:match(":%s") then
				element.definition_label, element.definition_text = element.text:match("^(.-):%s+(.+)")
			elseif element.text:match("^%s*[%-:]%s") and nth_listitem > 0 then
				element.definition_text = element.text:match("^%s*[%-:]%s+(.+)")
			elseif element.text:match(":$") then
				element.definition_label = element.text:match("^(.-):$")
				element.uri_is_for_definition_label = true
			end
			if element.definition_label then
				if element.definition_label:match("^%s*$") then
					element.definition_label = nil
					element.uri_is_for_definition_label = nil
				end
			end
			if element.definition_text then
				if element.definition_text:match("^%s*$") then
					element.definition_text = nil
				end
			end
			o[#o+1] = element
			nth_listitem = nth_listitem+1
		else
			nth_listitem = 0
			o[#o+1] = element
		end
	end
	return o
end

local function gemtext_extract_decoration(elements)
	o = {}
	local decorationclass = nil
	for n, element in ipairs(elements) do
		if element._ == "paragraph" and element.text then
			if element.text:match("^%$decoration%s.+") then
				decorationclass = element.text:match("^%$decoration%s+([^%s]+)")
			elseif element.text:match("^%$[a-z]+") then
				o[#o+1] = element
			else
				if decorationclass then
					element.decorationclass = decorationclass
					decorationclass = nil
				end
				o[#o+1] = element
			end
		else
			if decorationclass then
				element.decorationclass = decorationclass
				decorationclass = nil
			end
			o[#o+1] = element
		end
	end
	return o
end

local function gemtext_extract_index_id(elements)
	o = {}
	local index_id = nil
	for n, element in ipairs(elements) do
		if element._ == "paragraph" and element.text then
			if element.text:match("^%$id%s.+") then
				index_id = element.text:match("^%$id%s+([^%s]+)")
			elseif element.text:match("^%$[a-z]+") then
				o[#o+1] = element
			else
				if index_id then
					element.index_id = index_id
					index_id = nil
				end
				o[#o+1] = element
			end
		else
			if index_id then
				element.index_id = index_id
				index_id = nil
			end
			o[#o+1] = element
		end
	end
	return o
end

local function semantic_gemtext_elements_to_tree(elements, title, uri)
	local elements = gemtext_parse_inline_links(elements)
	elements = extract_list_headers(elements)
	elements = gemtext_extract_quotes(elements)
	elements = gemtext_extract_definitions(elements)
	elements = gemtext_extract_section_descriptions(elements)
	elements = gemtext_extract_decoration(elements)
	elements = gemtext_extract_index_id(elements)
	return gemtext_elements_to_document_tree(elements, title, uri)
end

------------------------------------------------
--- Gemini Markup Parser ---------------------
--------------------------------------------

local function parse_gemini_markup(text)
	assert(type(text) == "string", "Parser needs a string as first argument")
	local preformatted_block = false
	local gemtext_elements = {}
	for line in text:gmatch("[^\n]*") do
		local is_text = true
		if line:match("^```") then
			preformatted_block = not preformatted_block
			local alttext = line:match("^```%s*(.-)%s*$")
			if alttext == "" then
				alttext = nil
			end
			if preformatted_block then
				gemtext_elements[#gemtext_elements+1] = {_="preformatted", prefix=alttext, text=""}
			else
				gemtext_elements[#gemtext_elements].suffix = alttext
			end
			is_text = false
		end
		if (not preformatted_block) and is_text then
			if line:match("^###") then
				is_text = false
				gemtext_elements[#gemtext_elements+1] = {_="header", level=3, text=line:match("^###%s*(.-)%s*$")}
			elseif line:match("^##") then
				is_text = false
				gemtext_elements[#gemtext_elements+1] = {_="header", level=2, text=line:match("^##%s*(.-)%s*$")}
			elseif line:match("^#") then
				is_text = false
				gemtext_elements[#gemtext_elements+1] = {_="header", level=1, text=line:match("^#%s*(.-)%s*$")}
			elseif line:match("^>") then
				is_text = false
				gemtext_elements[#gemtext_elements+1] = {_="quote", text=line:match("^>%s*(.-)%s*$")}
			elseif line:match("^%*") then
				is_text = false
				gemtext_elements[#gemtext_elements+1] = {_="listitem", text=line:match("^%*%s*(.-)%s*$")}
			elseif line:match("^=>") then
				local uri,description = line:match("^=>%s*([^%s]+)%s*(.*)$")
				if uri then
					if description == "" then
						description = nil
					end
					is_text = false
					gemtext_elements[#gemtext_elements+1] = {_="link", text=description, uri=uri}
				end
			end
		end
		if is_text then
			if preformatted_block then
				if gemtext_elements[#gemtext_elements].text ~= "" then
					gemtext_elements[#gemtext_elements].text = gemtext_elements[#gemtext_elements].text.."\n"
				end
				gemtext_elements[#gemtext_elements].text = gemtext_elements[#gemtext_elements].text..line
			else
				if line == "" then
					gemtext_elements[#gemtext_elements+1] = {_="empty_line"}
				else
					gemtext_elements[#gemtext_elements+1] = {_="paragraph", text=line}
				end
			end
		end
	end
	return gemtext_elements
end

return {
	parse_gemini_markup = parse_gemini_markup,
	gemtext_element_paragraph_to_document_tree = gemtext_element_paragraph_to_document_tree,
	gemtext_extract_section_descriptions = gemtext_extract_section_descriptions,
	gemtext_parse_inline_links = gemtext_parse_inline_links,
	semantic_gemtext_elements_to_tree = semantic_gemtext_elements_to_tree,
}
