local function parse_uri(uri)
	this = {}
	local rest,query,index = uri:match("^([^#?]*)%??([^#]*)#?(.*)$")
	this.query = query
	this.index = index
	local scheme,rest_alt = rest:match("^([^:/]+):(.*)")
	this.scheme = scheme
	rest = rest_alt or rest
	local authority,rest_alt = rest:match("^//([^/]+)(.*)")
	this.path = rest_alt or rest
	this.authority = authority
	return this
end

return {
	parse_uri = parse_uri
}
