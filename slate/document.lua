local function pretty_print_element(element, pfx)
	local pfx=pfx or "|"
	if type(element) == "table" then
		print(pfx.."["..(element.type or "*").."."..(element.useclass or "*").."]")
		for k,v in pairs(element) do
			if not (k == "type" or k == "useclass" or k == "children") then
				print(pfx..tostring(k).." = "..tostring(v))
			end
		end
		if element.children then
			local n = #element.children
			for k,v in ipairs(element.children) do
				local kpfx = pfx
				local vpfx = pfx
				if k == n then
					kpfx = kpfx:gsub("(|)$","'")
					vpfx = vpfx:gsub("(|)$"," ")
				end
				print(kpfx.."-. "..tostring(k))
				pretty_print_element(v, vpfx.." |")
			end
		end
	elseif type(element) == "string" then
		print(pfx..(element:gsub("\n","\n"..pfx)))
	else
		print(pfx..tostring(element))
	end
end

function document_to_article(document)
	assert(type(document) == "table", "Document must be a table!")
	if type(document.children) == "table" then
		if #document.children == 1 then
			return document.children[1]
		else
			document.type = "section"
			document.useclass = "article"
			return document
		end
	end
	return nil
end

--if replace_func returns a non nil value the element will get replaced
local function replace_nodes(element, replace_func, replace_func_arg)
	if type(element) == "string" then return end
	assert(type(element) == "table", "replace_nodes needs an element as its first argument!")
	assert(type(replace_func) == "function", "replace_nodes needs a function as its second argument!")
	local children = element.children
	if children then
		for i = 1,#children do
			local child = children[i]
			local res = replace_func(child, replace_func_arg)
			if res then
				children[i] = res
			else
				replace_nodes(child, replace_func, replace_func_arg)
			end
		end
	end
end

local function include_replace_function(element, arg)
	if type(element) == "table" then
		if element.type == "paragraph" and element.useclass == nil then
			if type(element.children) == "table" then
				if #element.children == 1 and type(element.children[1]) == "string" then
					local path = element.children[1]:match("^%$include (.+)$")
					if path then
						return arg.loader(path, arg.loader_arg)
					end
				end
			end
		end
	end
end

return {
	pretty_print_element = pretty_print_element,
	document_to_article = document_to_article,
	replace_nodes = replace_nodes,
	include_replace_function = include_replace_function,
}
